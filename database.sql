DROP DATABASE IF EXISTS `trackers`;

CREATE DATABASE IF NOT EXISTS `trackers` CHARACTER SET 'UTF8';

USE `trackers`;

CREATE TABLE IF NOT EXISTS `users` (
  `id` INT PRIMARY KEY AUTO_INCREMENT,
  `name` VARCHAR(200),
  `email` VARCHAR(200) UNIQUE,
  `password` VARCHAR(200),
  `created_at` DATETIME
)ENGINE=MYISAM;

CREATE TABLE IF NOT EXISTS `volunteers` (
  `id` INT PRIMARY KEY AUTO_INCREMENT,
  `name` VARCHAR(200),
  `email` VARCHAR(200),
  `started` TINYINT(1) DEFAULT 0,
  `user_id` INT,
  `created_at` DATETIME,
  CONSTRAINT FK_users_volunteers FOREIGN KEY (user_id) REFERENCES volunteers(id) ON DELETE CASCADE
)ENGINE=MYISAM;

CREATE TABLE IF NOT EXISTS `tasks` (
  `id` INT PRIMARY KEY AUTO_INCREMENT,
  `title` VARCHAR(200),
  `volunteer_id` INT,
  `created_at` DATETIME,
  CONSTRAINT FK_volunteers_tasks FOREIGN KEY (volunteer_id) REFERENCES volunteers(id) ON DELETE CASCADE
) ENGINE=MYISAM;
