# ADEI ELEVATE - PROJECT

Bonjour à tous, Ceci est une mise à jour de l'application MPA qui devra être transformée en **SPA**. C'est une application de suivie d'activité de bénévolat.

## Le principe

- Démarrez l'application et elle vous demandera de vous authentifiez ou de créer un compte.
- Ensuite après authentification vous accédez à votre page d'accueil où vous pouvez ajouter au tant de bénévole que vous vouliez.
- Enfin l'application affichera la liste des bénévoles, supprimer.
- Le bouton `Détail` vous permet d'afficher les informations sur le bénévole et ainsi lui ajouté les taches qu'il aura terminé au fur et à mesure de son parcours (qui représente le tracking).
- Le bouton `ajouter un volontaire` vous permet comme son nom l'indique d'ajouter un volontaire (bénévole) à votre liste.
- Le bouton `accueil` vous permet comme son nom l'indique d'aller à la page d'accueil.
- Le champ `recherche` vous permet de faire des recherches sur les volontaires ajoutés.

**Votre exercice consistera à reconstruire cette application dans un modèle SPA et de mettre en place tout les éléments du développement moderne en mode AGILE.**

## Agile

Vous pouvez consulter la User Story dans le fichier `BACKLOGS.md`. Les taches sont aussi associées à la chaque Story.

## Installation du MPA

Importez le fichier `database.sql` dans PhpMyAdmin ou votre gestionnaire SQL équivalant. Ensuite éxècutez la commande suivante dans le dossier de votre application.

Dans le fichier `config/index.js` vous trouverez la configuration de la base de donnée et certain chose qui sont documentées. Changer ce qui est nécessaire en fonction de vous et lancer l'application.

```bash
# Installer les dépendances de l'application
npm install

# Lancer l'application
DEBUG=project:* node ./bin/www
```

## Stack

La SPA utilisera le stack suivant:

- React Js
- Expressjs Nodejs framework
- MySql
- API
- Web Service

## Déploiement

L'application sera déployé sur **GCP**, en utilisant Docker pour isoler les Web Services.

- Docker
- Kerbernetes
- Google Cloud Platform

## Liste des pages

- [ ] Page de connexion d'utilisateur
- [ ] Page d'inscription d'utilisateur
- [ ] Page d'accueil de l'utilisateur
- [ ] Modal d'ajout de volontaire
- [ ] Page de détail sur un volontaire
  - [ ] Liste des taches
  - [ ] Formulaire d'ajout de tache
