const express = require('express');
const router = express.Router();
const volunteerRouter = require('./volunteer');
const md5 = require('md5');
const middleware = require('../middleware');

const makeGravatar = (email) => {
  return "https://www.gravatar.com/avatar/" + md5(email);
};

// Branchement du middleware de vérification d'authentification
router.use(middleware.auth());

/**
 * Affichage de la page d'accueil
 *
 * GET /
 */
router.get('/', function(req, res, next) {
  const { user, flash } = req.session;
  const { search } = req.query;
  const connexion = require('../config/connexion');
  let sql = "SELECT * FROM volunteers WHERE user_id = ?";
  let binding = [user.id];
  delete req.session.flash;

  if (search) {
    sql += " AND name LIKE ?";
    binding.push(`%${search}%`);
  }

  sql += " ORDER BY created_at;";

  connexion.query(sql, binding, (err, resulat) => {
    if (err) {
      console.log(err)
      req.session.flash = {
        message: 'Une erreur est survenue !',
        error: true
      };

      return res.redirect('back');
    }

    // Création du gravatar pour chaque volontaire
    const volunteers = (resulat || []).map(volunteer => {
      volunteer.avatar = makeGravatar(volunteer.email);
      return volunteer;
    });

    // Création du gravatar
    user.avatar = makeGravatar(user.email);

    res.render('index', {
      volunteers, 
      user, 
      search, 
      flash: flash || {}
    });
  });
});

/**
 * Logout controller
 * 
 * POST /logout
 */
router.post('/logout', (req, res) => {
  // Suppression du flag de connexin
  delete req.session.user;

  // On régénère la session
  req.session.regenerate((err) => {
    req.session.flash = {
      error: false,
      message: 'Merci pour votre visite !'
    };

    res.redirect('/login');
  });
});

// On greffe le module qui gère les volontaires
router.use('/volunteers', volunteerRouter);

module.exports = router;
