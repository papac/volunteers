const express = require('express');
const router = express.Router();
const middleware = require('../middleware');
const bcrypt = require('bcryptjs');

router.use(middleware.guest());

/**
 * Affichage de la page de login
 *
 * GET /login
 */
router.get('/', (req, res, next) => {
  const flash = req.session.flash || {};
  delete req.session.flash;
  res.render('login', { flash });
});

/**
 * Action de login
 *
 * POST /login
 */
router.post('/', (req, res, next) => {
  const { email, password } = req.body;

  // On vérifie l"email est correct
  if (typeof email === 'undefined') {
    req.session.flash = {
      error: true,
      message: 'Email est vide!'
    };

    return res.redirect('/login');
  }

  // Récupération de la connexion
  const connexion = require('../config/connexion');

  connexion.query(`
    SELECT * FROM users 
    WHERE email = ? LIMIT 1
    `, [connexion.escape(email)], (err, resultat) => {
      // On vérifie s'il y a pas d'erreur
      if (err) {
        req.session.flash = {
          message: 'Une erreur est survenue !',
          error: true
        };

        return res.redirect('back');
      }

      const user = resultat[0];

      // On compare le mot de passe de l'utilisateur
      if (
        typeof user === 'undefined' 
        || !bcrypt.compareSync(connexion.escape(password), user.password)
      ) {
        req.session.flash = {
          error: true,
          message: 'Email or Password error !'
        };

        return res.redirect('/login');
      }

      // Changement de la session de l'utilisateur
      req.session.user = user;

      return res.redirect('/');
    });
});

module.exports = router;
