const express = require('express');
const bcrypt = require('bcryptjs');
const router = express.Router();
const middleware = require('../middleware');

router.use(middleware.guest());

/**
 * Affichage de la page de register
 *
 * GET /register
 */
router.get('/', (req, res, next) => {
  const flash = req.session.flash || {};
  delete req.session.flash;
  res.render('register', { flash });
});

/**
 * Creation de compte
 *
 * POST /register
 */
router.post('/', (req, res, next) => {
  const { email, password, name, remember } = req.body;

  // Verification des informations de l'utilisateur
  if (typeof email === 'undefined' || typeof password === 'undefined' || typeof name === 'undefined') {
    return res.redirect('back');
  }

  // Construction des informations de l'utilisateur
  const connexion = require('../config/connexion');
  const binding = {
    name,
    email: connexion.escape(email),
    password: bcrypt.hashSync(connexion.escape(password)),
  };

  connexion.query(`
    INSERT INTO users 
    SET ?, created_at = NOW()
    `, binding, (err, resultat) => {
      if (err) {
        req.session.flash = {
          message: 'Une erreur est survenue !',
          error: true
        };

        return res.redirect('back');
      }

      // Ajout de message flash
      req.session.flash = {
        error: false,
        message: 'Connectez vous ' + name + '!'
      };

      // Redirection sur la page d'acceuil
      return res.redirect('/login');
    });
});

module.exports = router;
