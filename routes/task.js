const express = require('express');
const bcrypt = require('bcryptjs');
const router = express.Router();
const middleware = require('../middleware');

/**
 * Afficher la page de tracking d'un volontaire
 * 
 * POST /volunteers/:id/add-task
 * > id c'est l'identifiant du volontaire
 */
router.post('/:id/add-task', function(req, res, next) {
  const { id } = req.params;
  const { user, flash } = req.session;
  const { task } = req.body;
  const connexion = require('../config/connexion');

  // On crée une nouvelle tache pour le volontaire.
  connexion.query(
    'INSERT INTO tasks SET ?, created_at = NOW()', {
      title: task, 
      volunteer_id: id
    }, (err, resultat) => {
      if (err) {
        req.session.flash = {
          message: 'Une erreur est survenue !',
          error: true
        };

        return res.redirect('back');
      }

      req.session.flash = {
        error: false,
        message: 'Nouvelle tache ajoutée !'
      };

      return res.redirect('back');
  });
});


/**
 * Permet de supprimer un tache dans la liste des taches du volontaire
 * 
 * POST /volunteers/:id/tasks/:taskId/add-task
 * > id c'est l'identifiant du volontaire
 */
router.get('/:id/tasks/:taskId/delete', function(req, res, next) {
  const { id, taskId } = req.params;
  const { user, flash } = req.session;
  const connexion = require('../config/connexion');

  // On supprime avec SQL la tache du volontaire
  connexion.query(
    `DELETE FROM tasks 
    WHERE id = ? AND volunteer_id = ?`, [
      taskId, 
      id
    ], (err, resultat) => {
      if (err) {
        req.session.flash = {
          message: 'Une erreur est survenue !',
          error: true
        };

        return res.redirect('back');
      }

      req.session.flash = {
        error: false,
        message: 'Tache supprimée !'
      };

      return res.redirect('back');
    });
});

module.exports = router;
