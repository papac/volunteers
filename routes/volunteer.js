const express = require('express');
const router = express.Router();
const md5 = require('md5');

const makeGravatar = (email) => {
  return "https://www.gravatar.com/avatar/" + md5(email);
};

/**
 * Permet d'ajouter un volontaire
 *
 * POST /add
 */
router.post('/add', function(req, res, next) {
  const { user } = req.session;
  const connexion = require('../config/connexion');
  let {name, email} = req.body;

  connexion.query(`
    INSERT INTO volunteers 
    SET ?, created_at = NOW()
    `, {
    name, email, 
    user_id: user.id
  }, (err, resultat) => {
      req.session.flash = {
        error: false,
        message: 'Volontaire bien ajouté !'
      };

      res.redirect('back');
    });
});

/**
 * Afficher la page de tracking d'un volontaire
 *
 * GET /volunteers/:id
 * > id c'est l'identifiant du volontaire
 */
router.get('/:id', function(req, res, next) {
  const { id } = req.params;
  const { user, flash } = req.session;
  const connexion = require('../config/connexion');
  delete req.session.flash;

  // On récupère les informations sur le volontaire
  // ainsi que la date de démarrage du tracking
  connexion.query(`
    SELECT * FROM volunteers 
    WHERE volunteers.id = ? AND user_id = ?
    LIMIT 1
    `, [id, user.id], (err, resultat) => {
      if (err) {
        req.session.flash = {
          message: 'Une erreur est survenu !',
          error: true
        }
        return res.redirect('back');
      }

      if (resultat.length == 0) {
        res.status(404);
        return res.redirect('back');
      }

      let volunteer = resultat[0];

      // On construit le gravatar du volontaire
      volunteer.avatar = makeGravatar(volunteer.email);

      // On récupère toutes ces taches
      connexion.query(`
        SELECT * 
        FROM tasks 
        WHERE volunteer_id = ?;
      `, [id], (err, resultat) => {

        if (err) {
          req.session.flash = {
            message: 'Une erreur est survenu !',
            error: true
          }
          return res.redirect('back');
        }

        return res.render('single-volunteer', {
          volunteer, 
          user, 
          flash: flash || {}, 
          tasks: resultat || [],
          search: ''
        });
      });
  });
});

/**
 * Permet de supprimer le compte d'un volontaire
 *
 * GET /volunteers/:id/delete
 * > id c'est l'identifiant du volontaire
 */
router.get('/:id/delete', function(req, res, next) {
  const { user } = req.session;
  const { id } = req.params;

  // Récupération de la connexion
  const connexion = require('../config/connexion');

  connexion.query(`
    DELETE FROM volunteers 
    WHERE id = ? AND user_id = ?
    `, [id, user.id], (err, resultat) => {
    if (err) {
      req.session.flash = {
        error: true,
        message: "Impossible d'effectuer la suppression."
      }
    } else {
      req.session.flash = {
        error: false,
        message: "La suppression effectuée !"
      }
    }
    
    res.redirect('back');
  });
});

/**
 * On greffe ici le module qui permet de gérer les taches
 */
router.use(require('./task'));

module.exports = router;
